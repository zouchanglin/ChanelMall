package cn.chanel.mall.repository;

import cn.chanel.mall.entity.Orders;
import cn.chanel.mall.utils.KeyUtil;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OrdersRepositoryTest {

    @Resource
    private OrdersRepository orderRepository;

    @Test
    public void save(){
        val order = new Orders();
        order.setId(KeyUtil.genUniqueKey());
        order.setUserId("21212121");
        order.setStatus(2);
        order.setMoney(new BigDecimal("0.25"));
        order.setGoodId("12341241");
        order.setCreateTime(System.currentTimeMillis());
        order.setUpdateTime(System.currentTimeMillis());

        val save = orderRepository.save(order);
        assertNotNull(save);
    }

    @Test
    public void findAllByUser() {
        orderRepository.findAllByUserId("");
    }
}
