package cn.chanel.mall.repository;


import cn.chanel.mall.entity.Category;
import cn.chanel.mall.utils.KeyUtil;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryRepositoryTest {
    @Autowired
    private CategoryRepository repository;

    @After
    public void after() {
        repository.deleteAll();
    }

    @Before
    public void before(){
        repository.deleteAll();
    }

    @Test
    public void add(){
        val category1 = new Category();
        category1.setId(KeyUtil.genUniqueKey());
        category1.setName("香水");
        repository.save(category1);

        val category2 = new Category();
        category2.setId(KeyUtil.genUniqueKey());
        category2.setName("彩妆");
        repository.save(category2);

        val category3 = new Category();
        category3.setId(KeyUtil.genUniqueKey());
        category3.setName("护肤品");
        repository.save(category3);

        val categoryList = repository.findAll();
        assertEquals(3, categoryList.size());
    }

    @Test
    public void delete(){
        val category1 = new Category();
        category1.setId(KeyUtil.genUniqueKey());
        category1.setName("香水");
        repository.save(category1);

        assertEquals(1, repository.findAll().size());

        repository.delete(category1);
        assertEquals(0, repository.findAll().size());

        repository.save(category1);
        repository.deleteById(category1.getId());
        assertEquals(0, repository.findAll().size());
    }

    @Test
    public void find(){
        val category1 = new Category();
        category1.setId(KeyUtil.genUniqueKey());
        category1.setName("香水");
        repository.save(category1);

        val categoryRetOpt = repository.findById(category1.getId());
        val categoryRet = repository.getById(category1.getId());
        assertTrue(categoryRetOpt.isPresent());
        assertNotNull(categoryRet);
        assertEquals(category1, categoryRet);
        assertEquals(category1, categoryRetOpt.get());
    }
}
