package cn.chanel.mall.utils;

import lombok.val;
import org.junit.jupiter.api.Test;

public class KeyUtilTest {

    @Test
    void genUniqueKey() {
        for (int i = 0; i < 50; i++) {
            val key = KeyUtil.genUniqueKey();
            System.out.println("genUniqueKey -> " + key);
        }
    }

    @Test
    void genVerifyKey() {
        for (int i = 0; i < 50; i++) {
            val key = KeyUtil.genVerifyKey();
            System.out.println("genVerifyKey -> " + key);
        }
    }
}
