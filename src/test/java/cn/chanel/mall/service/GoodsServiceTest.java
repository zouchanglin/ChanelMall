package cn.chanel.mall.service;

import cn.chanel.mall.entity.Goods;
import cn.chanel.mall.exception.MallException;
import cn.chanel.mall.repository.GoodsRepository;
import cn.chanel.mall.utils.KeyUtil;
import cn.chanel.mall.vo.ProductListVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Random;

import static org.junit.Assert.*;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class GoodsServiceTest {
    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GoodsRepository goodsRepository;


    @Before
    public void before(){
        goodsRepository.deleteAll();
        for (int i = 0; i < 40; i++) {
            Goods goods = genNewGoods();
            goodsRepository.save(goods);
        }
    }

    @After
    public void after(){
        goodsRepository.deleteAll();
    }

    @Test(expected = MallException.class)
    public void getAllGoods() {
        var productListVO = goodsService.getAllGoods(1);
        assertEquals(40, productListVO.getRow());

        productListVO = goodsService.getAllGoods(4);
        assertEquals(10, productListVO.getRow());

        productListVO = goodsService.getAllGoods(3);
        assertEquals(14, productListVO.getRow());
        assertEquals(3, productListVO.getColumn());

        productListVO = goodsService.getAllGoods(100);
        assertEquals(1, productListVO.getRow());
        assertEquals(100, productListVO.getColumn());

        goodsService.getAllGoods(0);
        goodsService.getAllGoods(-1);
    }


    private Goods genNewGoods() {
        var goods = new Goods();
        goods.setId(KeyUtil.genUniqueKey());
        goods.setDescribe(
                "Block out the haters with the fresh adidas® Originals Kamal Windbreaker Jacket.");
        goods.setPrice(new BigDecimal("89.00"));
        goods.setImage("http://localhost:8080/assets/img/product/1.jpg");
        goods.setName("New Luxury Men's Slim Fit Shirt Short Sleeve");
        goods.setStock(2000);
        goods.setScore(4);
        goods.setCategory(KeyUtil.genUniqueKey());
        return goods;
    }
}
