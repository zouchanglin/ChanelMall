﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>购物商城</title>

    <link rel="stylesheet" href="/assets/css/vendor/vendor.min.css" />
    <link rel="stylesheet" href="/assets/css/plugins/plugins.min.css" />
    <link rel="stylesheet" href="/assets/css/style.min.css" />

</head>

<body>
<#include "./common/nav.ftl">

<!-- breadcrumb-section start -->
<nav class="breadcrumb-section theme1 breadcrumb-bg1">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-title text-center my-20">
                    <h3 class="title text-dark text-capitalize">购物商城</h3>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- breadcrumb-section end -->

<!-- product tab start -->
<div class="product-tab pb-70">
    <div class="container grid-wraper">
        <div class="grid-nav-wraper mb-30">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 mb-3 mb-md-0">
                    <nav class="shop-grid-nav">
                        <ul class="nav nav-pills align-items-center" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href=""
                                    role="tab" aria-controls="pills-home" aria-selected="true">
                                    <i class="fa fa-th"></i>
                                </a>
                            </li>
                            <li>
                                <span class="total-products text-capitalize">总${allGoods?size}共个商品</span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-12 col-md-6 position-relative">
                    <div class="shop-grid-button d-flex align-items-center">
                        <span class="sort-by">商品类别</span>
                        <button class="btn-dropdown d-flex justify-content-between" type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ${currentCategory.name} <span class="ion-android-arrow-dropdown"></span>
                        </button>
                        <div class="dropdown-menu shop-grid-menu" aria-labelledby="dropdownMenuButton">
                            <#list allCategories as myCategory>
                                <a class="dropdown-item" href="/goods/${myCategory.id}">${myCategory.name}&emsp;&emsp;${myCategory.describe}</a>
                            </#list>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- product-tab-nav end -->
        <div class="tab-content" id="pills-tabContent">
            <!-- first tab-pane -->
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row grid-view theme1">
                    <#list allGoods as goods>
                        <div class="col-sm-6 col-md-4 col-lg-3 mb-30">
                            <div class="card product-card">
                                <div class="card-body p-0">
                                    <div class="product-thumbnail position-relative">
                                        <span class="badge badge-danger top-left">New</span>
                                        <a>
                                            <img class="first-img" src="${goods.image}" alt="thumbnail">
                                        </a>
                                        <!-- product links -->
                                        <div class="product-links d-flex d-flex justify-content-between">
                                            <a class="pro-btn" href="/cart/add?goods=${goods.id}&num=1">加入购物车</a>
                                            <a class="pro-btn" href="/goods/single?id=${goods.id}">商品详情</a>
                                        </div>
                                        <!-- product links end-->
                                    </div>
                                    <div class="product-desc">
                                        <h3 class="title">${goods.name}</h3>
                                        <div class="star-rating my-10">
                                            <!-- 固定长度  -->
                                            <#list 1..goods.score as i>
                                                <span class="ion-ios-star"></span>
                                            </#list>
                                        </div>
                                        <h6 class="product-price">￥${goods.price}</h6>
                                    </div>
                                </div>
                            </div>
                            <!-- product-list End -->
                        </div>
                    </#list>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="overlay">
    <div class="scale"></div>
    <form class="search-box" action="/goods/${currentCategory.id}" method="get">
        <input type="text" name="key" placeholder="Search products..." />
        <button id="close" type="submit"><i class="ion-ios-search-strong"></i></button>
    </form>
    <button class="close"><i class="ion-android-close"></i></button>
</div>

<#include "./common/foot.ftl">
<script src="/assets/js/vendor/vendor.min.js"></script>
<script src="/assets/js/plugins/plugins.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
