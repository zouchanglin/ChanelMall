﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>关于我们</title>
    <link rel="stylesheet" href="/assets/css/vendor/vendor.min.css" />
    <link rel="stylesheet" href="/assets/css/plugins/plugins.min.css" />
    <link rel="stylesheet" href="/assets/css/style.min.css" />
</head>

<body>
<#include "./common/nav.ftl">

<!-- breadcrumb-section start -->
<nav class="breadcrumb-section theme1 breadcrumb-bg1">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-title text-center my-20">
                    <h3 class="title text-dark text-capitalize">关于我们</h3>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- breadcrumb-section end -->

<!-- product tab start -->
<section class="about-section pb-40">
    <div class="container grid-wraper">
        <div class="row">
            <div class="col-lg-6 mb-30">
                <div class="about-content">
                    <h2 class="title mb-20">Welcome To zonan</h2>
                    <p class="mb-20">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore aperiam fugit consequuntur
                        voluptatibus ex sint iure in, distinctio sed dolorem aspernatur veritatis repellendus dolorum
                        voluptate, animi libero officiis eveniet accusamus recusandae. Temporibus
                        amet ducimus sapiente voluptatibus autem dolorem magnam quas, officiis eveniet accusamus animi
                        libero officiis eveniet accusamus recusandae. Temporibus
                    </p>
                    <p>
                        Sint voluptatum beatae necessitatibus quos mollitia vero, optio asperiores aut tempora iusto eum
                        rerum, possimus, minus quidem ut saepe laboriosam. Praesentium aperiam accusantium minus
                        repellendus accusamus neque iusto pariatur laudantium provident quod
                    </p>
                </div>
            </div>
            <div class="col-lg-6 mb-30">
                <div class="about-left-image mb-30">
                    <img src="/assets/img/blog-post/5.jpg" alt="img" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mb-30">
                <div class="about-info">
                    <h4 class="title mb-20">Our Company</h4>
                    <p>
                        Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur
                        adipisicing elit.
                    </p>
                </div>
            </div>
            <div class="col-md-4 mb-30">
                <div class="about-info">
                    <h4 class="title mb-20">Our Team</h4>
                    <p>
                        Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur
                        adipisicing elit.
                    </p>
                </div>
            </div>
            <div class="col-md-4 mb-30">
                <div class="about-info">
                    <h4 class="title mb-20">Testimonial</h4>
                    <p>
                        Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur
                        adipisicing elit.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- product tab end -->
<#include "./common/foot.ftl">
<#include "./common/search.ftl">
<script src="/assets/js/vendor/vendor.min.js"></script>
<script src="/assets/js/plugins/plugins.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>

</html>
