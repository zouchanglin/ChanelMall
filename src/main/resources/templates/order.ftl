﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="description" content="" />
    <title>我的订单</title>
    <link rel="stylesheet" href="/assets/css/vendor/vendor.min.css" />
    <link rel="stylesheet" href="/assets/css/plugins/plugins.min.css" />
    <link rel="stylesheet" href="/assets/css/style.min.css" />

</head>

<body>

<#include "./common/nav.ftl">

<!-- breadcrumb-section start -->
<nav class="breadcrumb-section theme1 breadcrumb-bg1">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-title text-center my-20">
                    <h3 class="title text-dark text-capitalize">我的订单</h3>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- breadcrumb-section end -->

<!-- product tab start -->
<section class="whish-list-section theme1 pb-70">
    <div class="container grid-wraper">
        <div class="row">
            <div class="col-12">
                <h3 class="title pb-25 text-center text-md-left text-capitalize">订单详情</h3>
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center" scope="col">订单号</th>
                                <th class="text-center" scope="col">商品</th>
                                <th class="text-center" scope="col">名称</th>
                                <th class="text-center" scope="col">数量</th>
                                <th class="text-center" scope="col">状态</th>
                                <th class="text-center" scope="col">总价</th>
                                <th class="text-center" scope="col">创建时间</th>
                                <th class="text-center" scope="col">更新时间</th>
                            </tr>
                        </thead>
                        <tbody>
                            <#list orderVOList as orders>
                            <tr>
                                <td class="text-center">
                                    <span class="whish-title">${orders.id} &nbsp;&nbsp;&nbsp;</span>
                                </td>
                                <th class="text-center" scope="row">
                                    <img src="${orders.goods.image}" alt="img">
                                </th>
                                <td class="text-center">
                                    <span class="whish-title">${orders.goods.name}</span>
                                </td>
                                <td class="text-center">
                                    <div class="product-count style">
                                        <div class="count d-flex justify-content-center">
                                            <input disabled type="number" min="1" value="${orders.num}" name="num">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="whish-list-price">
                                        ${orders.status}
                                    </span>
                                </td>
                                <td class="text-center">
                                    <span class="whish-list-price">
                                        ￥${orders.goods.price}
                                    </span>
                                </td>

                                <td class="text-center">
                                    <span class="whish-list-price">
                                        ${orders.createTime}
                                    </span>
                                </td>
                                <td class="text-center">
                                    <span class="whish-list-price">
                                        ${orders.updateTime}
                                    </span>
                                </td>
                            </tr>
                            </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- product tab end -->
<#include "./common/foot.ftl">
<#include "./common/search.ftl">
<script src="/assets/js/vendor/vendor.min.js"></script>
<script src="/assets/js/plugins/plugins.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
