﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>商品详情页</title>
    <link rel="stylesheet" href="/assets/css/vendor/vendor.min.css" />
    <link rel="stylesheet" href="/assets/css/plugins/plugins.min.css" />
    <link rel="stylesheet" href="/assets/css/style.min.css" />
</head>

<body>

<#include "./common/nav.ftl">

<!-- breadcrumb-section start -->
<nav class="breadcrumb-section theme1 breadcrumb-bg1">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb-title text-center my-20">
          <h3 class="title text-dark text-capitalize">商品详情</h3>
        </div>
      </div>
    </div>
  </div>
</nav>
<!-- breadcrumb-section end -->
<!-- product-single start -->
<form action="/cart/add" method="get">
  <input type="text" hidden value="${goods.id}" name="goods"/>
  <section class="product-single theme1">
  <div class="container grid-wraper">
    <div class="row">
      <div class="col-md-9 mx-auto col-lg-6 mb-5 mb-lg-0">
        <div class="position-relative">
          <span class="badge badge-danger top-left">New</span>
        </div>

        <div class="product-sync-init mb-30">
            <div class="single-product">
              <img src="${goods.image}" alt="product-thumb">
            </div>
        </div>

      </div>
      <div class="col-lg-6 mt-5 mt-md-0">
        <div class="single-product-info">
          <div class="single-product-head">
            <h2 class="title mb-20">${goods.name}</h2>
            <div class="star-content mb-20">
              <#list 1..goods.score as i>
                <span class="star-on"><i class="ion-ios-star"></i> </span>
              </#list>
              <a id="write-comment">
                <span class="ml-2"><i class="far fa-comment-dots"></i></span>
                Read reviews <span></span>
              </a>
              <a data-toggle="modal" data-target="#exampleModalCenter">
                <span class="edite"><i class="far fa-edit"></i></span> Write a review
              </a>
            </div>
          </div>
          <div class="product-body mb-40">
            <div class="d-flex align-items-center mb-30 border-bottom pb-30">
              <h6 class="product-price mr-20">
                <!--<del class="del">$23.90</del>-->
                <span class="onsale">￥${goods.price}</span>
              </h6>
              <!--<span class="badge my-badge position-static bg-dark">Save 10%</span>-->
            </div>
            <p class="font-size">
              ${goods.describe}
            </p>
            <ul class="font-size">
              <li>${goods.describe}</li>
              <li>${goods.describe}</li>
              <li>${goods.describe}</li>
            </ul>
          </div>
          <div class="product-footer">
            <div
              class="product-count style d-flex flex-column flex-sm-row mt-30 mb-30">
              <div class="count d-flex">
                <input type="number" min="1" max="30" step="1" value="1" name="num"/>
                <!--<div class="button-group">
                  <button class="count-btn increment">
                    <i class="fas fa-chevron-up"></i>
                  </button>
                  <button class="count-btn decrement">
                    <i class="fas fa-chevron-down"></i>
                  </button>
                </div>-->
              </div>
              <div>
                <input type="submit" class="btn theme-btn--dark3 btn--xl mt-30 mt-sm-0 btn-success" value="加入购物车">
              </div>
            </div>

            <div class="pro-social-links mt-30">
              <ul class="d-flex align-items-center">
                <li class="share">Share</li>
                <li>
                  <a href="#"><i class="ion-social-facebook"></i></a>
                </li>
                <li>
                  <a href="#"><i class="ion-social-twitter"></i></a>
                </li>
                <li>
                  <a href="#"><i class="ion-social-google"></i></a>
                </li>
                <li>
                  <a href="#"><i class="ion-social-pinterest"></i></a>
                </li>
              </ul>
            </div>
          </div>
          <div class="block-reassurance">
            <ul>
              <li>
                <img
                  src="/assets/img/icon/10.png"
                  alt="img"
                />
                Security policy (edit with Customer reassurance module)
              </li>
              <li>
                <img
                  src="/assets/img/icon/11.png"
                  alt="img"
                />
                Delivery policy (edit with Customer reassurance module)
              </li>
              <li>
                <img
                  src="/assets/img/icon/12.png"
                  alt="img"
                />
                Return policy (edit with Customer reassurance module)
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</form>
<br><br>
<!-- product-single end -->

<#include "./common/foot.ftl">
<script src="/assets/js/vendor/vendor.min.js"></script>
<script src="/assets/js/plugins/plugins.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
