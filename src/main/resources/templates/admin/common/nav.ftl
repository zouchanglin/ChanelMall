<#--<nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">-->
<nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
<#--    <ul class="nav sidebar-nav">-->
    <ul class="nav sidebar-nav">
        <li class="sidebar-brand">
            <a disabled="true">
                商城管理系统
            </a>
        </li>

        <li class="dropdown open">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <i class="fa fa-fw fa-plus"></i>用户管理<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header">用户相关</li>
                <li><a href="/admin/user/list">用户列表</a></li>
            </ul>
        </li>

        <li class="dropdown open">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <i class="fa fa-fw fa-plus"></i>分类管理<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header">商品分类</li>
                <li><a href="/admin/category/list">分类列表</a></li>
                <li><a href="/admin/category/add">新增分类</a></li>
            </ul>
        </li>

        <li class="dropdown open">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <i class="fa fa-fw fa-plus"></i>商品管理<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header">商品相关</li>
                <li><a href="/admin/goods/list">商品列表</a></li>
                <li><a href="/admin/goods/add">新增商品</a></li>
            </ul>
        </li>

        <li class="dropdown open">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <i class="fa fa-fw fa-plus"></i>订单管理<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header">订单相关</li>
                <li><a href="/admin/order/list">订单列表</a></li>
            </ul>
        </li>

        <li class="dropdown open">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <i class="fa fa-fw fa-plus"></i>其他管理<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <a href="/h2"><i class="fa fa-fw fa-list-alt"></i> H2数据管理</a>
                <a href="#"><i class="fa fa-fw fa-list-alt"></i>关于</a>
            </ul>
        </li>
    </ul>
</nav>
