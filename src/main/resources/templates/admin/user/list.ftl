<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    用户信息列表 <small>List of User</small>
                </h4>
            </div>
            <div class="row">
                <form action="/admin/user/list" method="get">
                <div class="col-lg-6">
                    <div class="input-group">

                            <input type="text" class="form-control" name="key" placeholder="输入ID或者邮箱进行搜索...">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit">搜索用户</button>
                            </span>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
                </form>

            </div><!-- /.row -->
            <hr>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        用户编号
                    </th>
                    <th>
                        用户邮箱
                    </th>
                    <th>
                        用户名称
                    </th>
                    <th>
                        注册时间
                    </th>
                </tr>
                </thead>
                <tbody>
                <#list allUser as user>
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.email}</td>
                        <td>${user.name}</td>
                        <td>${user.createTime}</td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
