<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    订单详情（在此更新订单状态） <small>Update Order</small>
                </h4>
            </div>
            <hr>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <form role="form" action="/admin/order/update" method="post">
                        <input type="text" name="id" hidden value="${adminOrderVO.orderVO.id}">
                        <div class="form-group">
                            <label for="exampleInputEmail1">订单编号</label>
                            <input type="text" step="0.01" disabled class="form-control" id="exampleInputEmail1"
                                   value="${adminOrderVO.orderVO.id}"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">订单金额</label>
                            <input type="text" disabled class="form-control" id="exampleInputEmail1"
                                   value="${adminOrderVO.orderVO.money}"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">订单状态</label>
                            <select class="form-select" name="status" id="exampleInputPassword1">
                                <#list allOrderStatus as orderStatus>
                                    <option value="${orderStatus.code}" <#if orderStatus.code == adminOrderVO.statusCode>selected</#if>>${orderStatus.name}</option>
                                </#list>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">订单商品</label>
                            <input type="text" disabled class="form-control" id="exampleInputEmail1"
                                   value="${adminOrderVO.orderVO.goods.name} × ${adminOrderVO.orderVO.num} （件）"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">订单创建时间</label>
                            <input type="text" disabled class="form-control" id="exampleInputEmail1"
                                   value="${adminOrderVO.orderVO.createTime}"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">订单更新时间</label>
                            <input type="text" disabled class="form-control" id="exampleInputEmail1"
                                   value="${adminOrderVO.orderVO.updateTime}"/>
                        </div>
                        <button type="submit" class="btn btn-default">修改商品</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
