<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    订单列表 <small>List of Order</small>
                </h4>
            </div>
            <div class="row">
                <form action="/admin/order/list" method="get">
                <div class="col-lg-6">
                    <div class="input-group">
                            <input type="text" class="form-control" name="key" placeholder="输入ID或者邮箱进行搜索...">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit">搜索订单</button>
                            </span>
                    </div>
                </div>
                </form>
            </div>
            <hr>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        订单编号
                    </th>
                    <th>
                        订单状态
                    </th>
                    <th>
                        订单客户
                    </th>
                    <th>
                        订单创建时间
                    </th>
                    <th>
                        订单更新时间
                    </th>
                    <th>

                    </th>
                </tr>
                </thead>
                <tbody>
                <#list allOrders as adminOrder>
                    <tr>
                        <td>${adminOrder.orderVO.id}</td>
                        <td>${adminOrder.orderVO.status}</td>
                        <td>${adminOrder.user.email}</td>
                        <td>${adminOrder.orderVO.createTime}</td>
                        <td>${adminOrder.orderVO.updateTime}</td>
                        <td>
                            <a class="btn theme--btn-default" href="/admin/order/detail?id=${adminOrder.orderVO.id}">订单详情</a>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
