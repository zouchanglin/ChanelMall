<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>管理员登录</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/signin.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <form class="form-signin" action="/admin/login" method="post">
        <h3 class="form-signin-heading">欢迎管理员登录</h3>
        <label for="inputEmail" class="sr-only">账号</label>
        <input type="text" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name="username">
        <label for="inputPassword" class="sr-only">密码</label>
        <input  style="margin-top: 10px" type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">

        <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
    </form>

</div> <!-- /container -->
</body>
</html>
