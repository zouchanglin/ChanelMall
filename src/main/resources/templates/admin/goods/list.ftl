<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    商品列表 <small>List of Goods</small>
                </h4>
            </div>
            <div class="row">
                <form action="/admin/goods/list" method="get">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <input type="text" class="form-control" name="key" placeholder="输入商品名称进行搜索...">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit">搜索商品</button>
                            </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                </form>

            </div><!-- /.row -->
            <hr>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        商品编号
                    </th>
                    <th>
                        商品图片
                    </th>
                    <th>
                        商品名称
                    </th>
                    <th>
                        商品描述
                    </th>
                    <th>
                        价格
                    </th>
                    <th>
                        评分
                    </th>
                    <th>
                        库存
                    </th>
                    <th>
                        分类于
                    </th>
                    <th>

                    </th>
                </tr>
                </thead>
                <tbody>
                <#list allGoods as goods>
                    <tr>
                        <td>${goods.id}</td>
                        <td>
                            <img src="${goods.image}" alt="" style="width: 70px; height: 70px">
                        </td>
                        <td>${goods.name}</td>
                        <td><p class="text-overflow">${goods.describe}</p></td>
                        <td>${goods.price}￥</td>
                        <td>${goods.score}</td>
                        <td>${goods.stock?c}</td>
                        <td>${goods.categoryEntity.name}</td>
                        <td>
                            <a class="btn btn-info" href="/admin/goods/update?goods=${goods.id}">修改商品</a>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style>
    .text-overflow {
        display:block;
        width:28em;
        word-break:keep-all;
        white-space:nowrap;
        overflow:hidden;
        text-overflow:ellipsis;
    }
</style>
</body>
</html>
