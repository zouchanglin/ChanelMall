<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    修改商品 <small>Update Goods</small>
                </h4>
            </div>
            <hr>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <form role="form" action="/admin/goods/update" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="exampleInputEmail1">商品原图片</label>
                            <img src="${goods.image}" alt="" style="height:150px; width: 150px">
                            <input type="text" value="${goods.image}" hidden name="oldImageUrl">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">上传商品图片 (不上传默认使用原图)</label>
                            <input class="btn theme--btn-default" type="file" name="file" id="exampleInputFile" />
                            <p class="help-block">
                                The file must be less than 50M! 图片不大于50Mb！
                            </p>
                        </div>
                        <input type="text" hidden name="id" value="${goods.id}" />
                        <div class="form-group">
                            <label for="exampleInputEmail1">商品名称</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="${goods.name}" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">商品库存</label>
                            <input type="number" class="form-control" id="exampleInputEmail1" name="stock" value="${goods.stock}"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">商品价格</label>
                            <input type="number" step="0.01" class="form-control" id="exampleInputEmail1" name="price" value="${goods.price}"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">商品描述</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="describe" value="${goods.describe}"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">商品评分</label>
                            <select class="form-select" name="score" id="exampleInputPassword1">
                                <option value="1" <#if goods.score == 1>selected</#if>>1</option>
                                <option value="2" <#if goods.score == 2>selected</#if>>2</option>
                                <option value="3" <#if goods.score == 3>selected</#if>>3</option>
                                <option value="4" <#if goods.score == 4>selected</#if>>4</option>
                                <option value="5" <#if goods.score == 5>selected</#if>>5</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">商品分类</label>
                            <select class="form-select" name="category" id="exampleInputPassword1">
                                <#list allCategories as category>
                                    <option value="${category.id}" <#if goods.category == category.id>selected</#if>>${category.name}</option>
                                </#list>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">修改商品</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
