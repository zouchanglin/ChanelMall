<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    添加商品 <small>Add Goods</small>
                </h4>
            </div>
            <hr>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <form role="form" action="/admin/goods/add" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="exampleInputFile">上传商品图片</label>
                            <input class="btn theme--btn-default" type="file" name="file" id="exampleInputFile" />
                            <p class="help-block">
                                The file must be less than 50M! 图片不大于50Mb！
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">商品名称</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="name" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">商品库存</label>
                            <input type="number" class="form-control" id="exampleInputEmail1" name="stock" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">商品价格</label>
                            <input type="number" step="0.01" class="form-control" id="exampleInputEmail1" name="price" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">商品描述</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="describe"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">商品评分</label>
                            <select class="form-select" name="score" id="exampleInputPassword1">
                                    <option value="1">1</option>
                                    <option value="1">2</option>
                                    <option value="1">3</option>
                                    <option value="1">4</option>
                                    <option value="1">5</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">商品分类</label>
                            <select class="form-select" name="category" id="exampleInputPassword1">
                                <#list allCategories as category>
                                    <option value="${category.id}">${category.name}</option>
                                </#list>
                            </select>
                        </div>
                         <button type="submit" class="btn btn-default">添加商品</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
