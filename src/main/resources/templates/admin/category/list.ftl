<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    商品分类列表 <small>List of Category</small>
                </h4>
            </div>
            <hr>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        类别编号
                    </th>
                    <th>
                        分类名称
                    </th>
                    <th>
                        分类描述
                    </th>
                </tr>
                </thead>
                <tbody>
                <#list allCategory as category>
                    <tr>
                        <td>${category.id}</td>
                        <td>${category.name}</td>
                        <td>${category.describe}</td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
