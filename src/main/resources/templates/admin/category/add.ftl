<html lang="zh-CN">
<#include "../common/header.ftl">
<body>
<div id="wrapper" class="toggled">
    <#--边栏sidebar-->
    <#include "../common/nav.ftl">
    <div id="page-content-wrapper" style="padding-top: 10px">
        <div class="col-md-12 column">
            <div class="page-header">
                <h4>
                    添加商品分类 <small>Add Category</small>
                </h4>
            </div>
            <hr>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <form role="form" action="/admin/category/add" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">分类名称</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="name" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">分类描述</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="describe"/>
                        </div>
                         <button type="submit" class="btn btn-default">新建分类</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
