<!-- header start -->
<header id="sticky" class="header style2 theme1">
    <!-- header-middle start -->
    <div class="header-middle px-xl-4">
        <div class="container position-relative">
            <div class="row align-items-center">
                <div class="col-9 col-xl-7 position-xl-relative">
                    <div class="d-flex align-items-center justify-content-lg-between">
                        <div class="logo mr-lg-5 mr-xl-0">
                            <a href="/goods/default"><img src="/assets/img/logo/logo-dark.jpg" alt="logo"></a>
                        </div>
                        <nav class="header-bottom theme1 d-none d-lg-block">
                            <ul class="main-menu d-flex align-items-center">
                                <li><a href="/goods/default">商城首页</a><li>
                                <li><a href="/about/us">关于我们</a></li>
                                <li><a href="/about/contact">联系我们</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-3 col-xl-5">
                    <!-- search-form end -->
                    <div class="d-flex align-items-center justify-content-end">
                        <div class="cart-block-links theme1">
                            <ul class="d-flex align-items-center">
                                <li>
                                    <#if userInfo ??>
                                    <span style="color: red">
                                        ${userInfo.name}
                                    </span>
                                    <#else >
                                        未登录
                                    </#if>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="search search-toggle">
                                        <i class="ion-ios-search-strong"></i>
                                    </a>
                                </li>
                                <li class="cart-block position-relative d-none d-sm-block">
                                    <a href="/cart/list">
                                        <i class="ion-bag"></i>
                                    </a>
                                </li>

                                <li class="cart-block position-relative d-none d-sm-block">
                                    <a href="/order/list">
                                        <i class="ion-android-menu"></i>
                                    </a>
                                </li>
                                <#if userInfo ??>
                                    <li class="mr-0 cart-block">
                                        <a href="/user/logout">
                                            <i class="ion-log-out"></i>
                                        </a>
                                    </li>
                                <#else>
                                    <li class="mr-0 cart-block">
                                        <a href="/user/login">
                                            <i class="ion-log-in"></i>
                                        </a>
                                    </li>
                                </#if>
                                <!-- cart block end -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header-middle end -->
</header>
<!-- header end -->
