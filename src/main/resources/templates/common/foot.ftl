<footer class="bg-lighten2 theme1 position-relative">
    <!-- footer bottom start -->
    <div class="footer-bottom pt-70 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-6 mb-30">
                    <div class="footer-widget">
                        <div class="footer-logo mb-30">
                            <a href="/goods/default">
                                <img src="/assets/img/logo/logo-dark.jpg" alt="footer logo">
                            </a>
                        </div>
                        <p class="text mb-35">We are a team of designers and developers that create high quality
                            Magento, Prestashop, Opencart.</p>
                        <div class="social-network">
                            <h2 class="title text mb-20 text-capitalize">Follow Us On Social:</h2>
                            <ul class="d-flex">
                                <li><a href="https://www.facebook.com/" target="_blank"><span
                                                class="ion-social-facebook"></span></a></li>
                                <li><a href="https://twitter.com/" target="_blank"><span
                                                class="ion-social-twitter"></span></a></li>
                                <li><a href="https://www.youtube.com/" target="_blank"><span
                                                class="ion-social-youtube-outline"></span></a></li>
                                <li><a href="https://www.youtube.com/" target="_blank"><span
                                                class="ion-social-googleplus"></span></a></li>
                                <li class="mr-0"><a href="https://www.instagram.com/" target="_blank"><span
                                                class="ion-social-instagram-outline"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-2 mb-30">
                    <div class="footer-widget">
                        <div class="section-title mb-20">
                            <h2 class="title text-dark text-capitalize">My Account</h2>
                        </div>
                        <address class="mb-0">
                            <span>Saturday - Thurs: 9AM - 6PM</span>
                            <span>Sat: 9AM-6PM</span>
                            <span>Friday Closed</span>
                            <span>We Work All The Holidays</span>
                        </address>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-2 mb-30">
                    <div class="footer-widget">
                        <div class="section-title mb-20">
                            <h2 class="title text-dark text-capitalize">Opening Time</h2>
                        </div>
                        <!-- footer-menu start -->
                        <ul class="footer-menu">
                            <li><a href="#">Delivery</a></li>
                            <li><a href="/about/us">About us</a></li>
                            <li><a href="#">Secure payment</a></li>
                            <li><a href="/about/contact">Contact us</a></li>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="#">Stores</a></li>
                        </ul>
                        <!-- footer-menu end -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-2 mb-30">
                    <div class="footer-widget">
                        <div class="section-title mb-20">
                            <h2 class="title text-dark text-capitalize">Information</h2>
                        </div>
                        <!-- footer-menu start -->
                        <ul class="footer-menu">
                            <li><a href="#">Legal Notice</a></li>
                            <li><a href="#">Prices drop</a></li>

                            <li><a href="#">New products</a></li>

                            <li><a href="#">Best sales</a></li>

                            <li><a href="/user/login">Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="coppy-right">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="border-top py-20">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4 col-xl-3 orders-last orders-md-first">
                                <div class="text-center text-lg-left">
                                    <p class="mb-3 mb-md-0">Copyright &copy; <a
                                                href="http://www.bootstrapmb.com/">HasThemes</a>. All
                                        Rights Reserved</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8 col-xl-9">
                                <ul
                                        class="footer-menu copyright-menu d-flex flex-wrap justify-content-center justify-content-md-end">
                                    <li><a href="#">Legal Notice</a></li>
                                    <li><a href="#">Prices drop</a></li>

                                    <li><a href="#">New products</a></li>

                                    <li><a href="#">Best sales</a></li>

                                    <li><a href="login.html">Login</a></li>

                                    <li><a href="myaccount.html">My account</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
