﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="description" content="" />
    <title>购物车</title>
    <link rel="stylesheet" href="/assets/css/vendor/vendor.min.css" />
    <link rel="stylesheet" href="/assets/css/plugins/plugins.min.css" />
    <link rel="stylesheet" href="/assets/css/style.min.css" />

</head>

<body>

<#include "./common/nav.ftl">

<!-- breadcrumb-section start -->
<nav class="breadcrumb-section theme1 breadcrumb-bg1">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-title text-center my-20">
                    <h3 class="title text-dark text-capitalize">购物车</h3>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- breadcrumb-section end -->

<!-- product tab start -->
<section class="whish-list-section theme1 pb-70">
    <div class="container grid-wraper">
        <div class="row">
            <div class="col-12">
                <h3 class="title pb-25 text-center text-md-left text-capitalize">你的购物车详情</h3>
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center" scope="col">商品</th>
                                <th class="text-center" scope="col">名称</th>
                                <th class="text-center" scope="col">数量</th>
                                <th class="text-center" scope="col">单价</th>
                                <th class="text-center" scope="col">操作</th>
                                <th class="text-center" scope="col">购买</th>
                            </tr>
                        </thead>
                        <tbody>
                            <#list cartVO.cartGoodsList as cart>
                            <tr>
                                <th class="text-center" scope="row">
                                    <img src="${cart.goods.image}" alt="img">
                                </th>
                                <td class="text-center">
                                    <span class="whish-title">${cart.goods.name}</span>
                                </td>
                                <form action="/pay" method="get">
<#--                                    <input type="text" value="${cart.goods.id}" name="goods" hidden>-->
                                    <input type="text" value="${cart.id}" name="cartId" hidden>
                                <td class="text-center">
                                    <div class="product-count style">
                                        <div class="count d-flex justify-content-center">
                                            <input type="number" min="1" disabled value="${cart.nums}">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="whish-list-price">
                                        ￥${cart.goods.price}
                                    </span></td>

                                <td class="text-center">
                                    <a href="/cart/delete?goods=${cart.id}"> <span class="trash"><i class="fas fa-trash-alt"></i> </span></a>
                                </td>
                                <td class="text-center">
                                    <input class="btn theme-btn--dark1 btn--xl text-uppercase" type="submit" value="购买"></input>
                                </td>
                                </form>
                            </tr>
                            </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- product tab end -->
<#include "./common/foot.ftl">
<#include "./common/search.ftl">
<script src="/assets/js/vendor/vendor.min.js"></script>
<script src="/assets/js/plugins/plugins.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
