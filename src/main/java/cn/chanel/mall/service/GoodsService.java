package cn.chanel.mall.service;

import cn.chanel.mall.admin.form.GoodsForm;
import cn.chanel.mall.admin.form.GoodsUpdateForm;
import cn.chanel.mall.vo.GoodsVO;
import cn.chanel.mall.vo.ProductListVO;
import java.util.List;

public interface GoodsService {
    /**
     * 获取全部商品
     * @param column 分几列展示
     * @return 商品展示 ViewObject
     */
    ProductListVO getAllGoods(int column);

    ProductListVO getAllGoodsByCategory(int column, String category);

    List<GoodsVO> getAllGoods();

    List<GoodsVO> getAllGoodsByCategory(String category);

    List<GoodsVO> getAllGoodsByCategoryAndKey(String category, String key);


    List<GoodsVO> getAllGoodsByKey(String key);

    void addNewGoods(String imageFileName, GoodsForm goodsForm);

    GoodsVO getGoodsById(String id);

    void updateGoods(String imageFileName, GoodsUpdateForm goodsForm);
}
