package cn.chanel.mall.service;

import cn.chanel.mall.admin.vo.AdminUserVO;
import cn.chanel.mall.entity.User;

import java.util.List;

public interface UserService {
    User loginOrRegister(String email, String password);

    User findUserById(String userId);

    List<AdminUserVO> getAllUser();

    List<AdminUserVO> getAllUserByKey(String key);
}
