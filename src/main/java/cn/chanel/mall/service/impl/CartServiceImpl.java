package cn.chanel.mall.service.impl;

import cn.chanel.mall.entity.Cart;
import cn.chanel.mall.entity.Goods;
import cn.chanel.mall.enums.ResultEnum;
import cn.chanel.mall.exception.MallException;
import cn.chanel.mall.repository.CartRepository;
import cn.chanel.mall.repository.GoodsRepository;
import cn.chanel.mall.repository.UserRepository;
import cn.chanel.mall.service.CartService;
import cn.chanel.mall.utils.KeyUtil;
import cn.chanel.mall.vo.CartVO;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;

@Slf4j
@Service
public class CartServiceImpl implements CartService {
    @Resource
    private CartRepository cartRepository;

    @Resource
    private GoodsRepository goodsRepository;

    @Resource
    private UserRepository userRepository;

    @Override
    public CartVO getUserCart(String userId) {
        if(StringUtils.hasText(userId)){
            val cartVO = new CartVO();
            cartVO.setUser(userId);
            val cartList = cartRepository.findAllByUser(userId);
            final ArrayList<CartVO.CartGoods> list = new ArrayList<>(cartList.size());
            for(val goods: cartList){
                final Goods getOne = goodsRepository.getById(goods.getGoods());
                final int nums = goods.getNums();
                list.add(convert(getOne, nums, goods));
            }
            cartVO.setCartGoodsList(list);
            return cartVO;
        }else {
            throw new MallException(ResultEnum.PARAM_ERROR);
        }
    }

    @Override
    public void addCart(String userId, String goodsId, int num) {
        var optUser = userRepository.findById(userId);
        if(optUser.isEmpty()){
            throw new MallException(ResultEnum.USER_INF_ERROR);
        }

        var optGoods = goodsRepository.findById(goodsId);
        if(optGoods.isEmpty()){
            throw new MallException(ResultEnum.PARAM_ERROR);
        }

        if(num <= 0){
            throw new MallException(ResultEnum.PARAM_ERROR);
        }

        var cart = cartRepository.findCartByUserAndGoods(userId, goodsId);
        if(cart != null){
            cart.setNums(cart.getNums() + num);
        }else {
            cart = new Cart();
            cart.setUser(userId);
            cart.setGoods(goodsId);
            cart.setNums(num);
            cart.setId(KeyUtil.genUniqueKey());
        }
        val save = cartRepository.save(cart);
        log.info("addCart save = {}", save);
    }

    @Override
    public void deleteUserCart(String cartId) {
        if(StringUtils.hasText(cartId)){
            cartRepository.deleteById(cartId);
        }else {
            log.info("deleteUserCart cart null ");
        }
    }

    private CartVO.CartGoods convert(Goods goods, int nums, Cart cart) {
        final CartVO.CartGoods cartGoods = new CartVO.CartGoods();
        cartGoods.setGoods(goods);
        cartGoods.setNums(nums);
        cartGoods.setId(cart.getId());
        return cartGoods;
    }
}
