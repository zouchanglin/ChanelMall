package cn.chanel.mall.service;

import cn.chanel.mall.vo.CartVO;

public interface CartService {
    /**
     * 根据用户 Id获取购物车信息
     * @param userId 用户 Id
     * @return {@link CartVO}
     */
    CartVO getUserCart(String userId);

    void addCart(String userId, String goodsId, int num);

    void deleteUserCart(String cartId);
}
