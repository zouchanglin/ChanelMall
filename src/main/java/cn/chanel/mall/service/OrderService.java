package cn.chanel.mall.service;

import cn.chanel.mall.admin.vo.AdminOrderVO;
import cn.chanel.mall.vo.OrderVO;

import java.util.List;

public interface OrderService {
    OrderVO createOrder(String cartId);

    List<OrderVO> getAllOrderByUser(String user);

    List<AdminOrderVO> getAllOrders();

    List<AdminOrderVO> getAllOrdersByKey(String key);

    AdminOrderVO getOrderVOById(String id);

    void updateOrder(String id, int status);
}
