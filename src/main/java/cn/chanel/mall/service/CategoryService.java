package cn.chanel.mall.service;

import cn.chanel.mall.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory();

    Category getCategoryById(String id);

    void addNewCategory(String name, String describe);
}
