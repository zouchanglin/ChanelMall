package cn.chanel.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChanelMallApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChanelMallApplication.class, args);
    }

}
