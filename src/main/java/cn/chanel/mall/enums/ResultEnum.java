package cn.chanel.mall.enums;

import lombok.Getter;

@Getter
public enum ResultEnum implements CodeEnum {
    SUCCESS(0, "请求成功"),
    PARAM_ERROR(1, "参数错误"),
    USER_INF_ERROR(2, "用户不存在"),
    GOODS_INF_ERROR(2, "商品不存在"),
    FILE_UPLOAD_NULL(3, "文件选择为空，上传失败"),
    ORDER_INF_ERROR(4, "订单不存在"),
    NEED_ADMIN_LOGIN(5, "需要管理员登录"),
    CATEGORY_INF_ERROR(6, "分类不存在");

    private final int code;
    private final String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
