package cn.chanel.mall.enums;

import lombok.Getter;

/**
 * 订单状态
 */
@Getter
public enum OrderStatusEnum implements CodeEnum {
    ORDER_CREATE(0, "新订单"),
    ORDER_PAY(1, "已支付"),
    ORDER_DELIVERY(2, "已发货"),
    ORDER_RECEIVE(3, "已签收"),
    ORDER_REFUND(4, "已退款");

    OrderStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private final int code;
    private final String name;
}
