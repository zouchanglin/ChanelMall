package cn.chanel.mall.vo;

import cn.chanel.mall.entity.Goods;
import lombok.Data;


@Data
public class OrderVO {
    private String id;

    /**
     * 订单总金额
     */
    private String money;

    /**
     * 订单状态（创建、已支付、已发货、已签收、已退款）
     */
    private String status;

    /**
     * 商品ID
     */
    private Goods goods;

    /**
     * 商品数量
     */
    private int num;

    /**
     * 订单创建时间戳
     */
    private String createTime;

    /**
     * 订单更新时间戳
     */
    private String updateTime;
}
