package cn.chanel.mall.vo;

import lombok.Data;

@Data
public class ProductListVO {
    /**
     * 多少列
     */
    private int column;

    /**
     * 多少行
     */
    private int row;

    /**
     * 商品数组
     */
    private GoodsVO[][] goodsVOS;
}
