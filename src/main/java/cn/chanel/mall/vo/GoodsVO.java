package cn.chanel.mall.vo;

import cn.chanel.mall.entity.Category;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GoodsVO {
    private String id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品库存
     */
    private long stock;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品图片链接
     */
    private String image;

    /**
     * 商品描述
     */
    private String describe;

    /**
     * 商品评分
     */
    private int score;

    /**
     * 商品类别
     */
    private String category;

    /**
     * 商品类别文字描述
     */
    private Category categoryEntity;
}
