package cn.chanel.mall.vo;

import cn.chanel.mall.entity.Goods;
import lombok.Data;

import java.util.List;

@Data
public class CartVO {
    /**
     * List < 商品 + 商品数量 >
     */
    private List<CartGoods> cartGoodsList;

    /**
     * 用户id
     */
    private String user;


    @Data
    public static class CartGoods {
        private String id;

        private Goods goods;

        private int nums;
    }
}
