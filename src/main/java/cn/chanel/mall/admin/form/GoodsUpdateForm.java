package cn.chanel.mall.admin.form;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsUpdateForm extends GoodsForm{
    private String id;
    private String oldImageUrl;
}
