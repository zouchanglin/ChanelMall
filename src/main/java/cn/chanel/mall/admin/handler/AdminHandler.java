package cn.chanel.mall.admin.handler;

import cn.chanel.mall.admin.expection.AdminException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import java.util.HashMap;

@ControllerAdvice
public class AdminHandler {
    @ExceptionHandler(value = AdminException.class)
    public ModelAndView handlerAdminException(AdminException e){
        final HashMap<String, Object> map = new HashMap<>();
        map.put("msg", e.getMessage());
        map.put("url", "/admin/login");
        return new ModelAndView("/admin/common/error", map);
    }
}
