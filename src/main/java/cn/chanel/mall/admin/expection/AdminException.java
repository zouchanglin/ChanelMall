package cn.chanel.mall.admin.expection;

import cn.chanel.mall.enums.ResultEnum;

public class AdminException extends RuntimeException{
    private final Integer code;

    public AdminException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public AdminException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
