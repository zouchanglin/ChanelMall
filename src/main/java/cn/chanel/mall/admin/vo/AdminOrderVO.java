package cn.chanel.mall.admin.vo;

import cn.chanel.mall.entity.User;
import cn.chanel.mall.vo.OrderVO;
import lombok.Data;

@Data
public class AdminOrderVO {
    private User user;

    private OrderVO orderVO;

    private int statusCode;
}
