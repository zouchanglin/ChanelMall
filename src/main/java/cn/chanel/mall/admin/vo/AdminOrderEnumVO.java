package cn.chanel.mall.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AdminOrderEnumVO {
    private String name;
    private int code;
}
