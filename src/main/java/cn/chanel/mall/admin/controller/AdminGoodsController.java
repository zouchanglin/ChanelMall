package cn.chanel.mall.admin.controller;

import cn.chanel.mall.admin.conf.AdminAccountConfig;
import cn.chanel.mall.admin.form.GoodsForm;
import cn.chanel.mall.admin.form.GoodsUpdateForm;
import cn.chanel.mall.constant.CookieName;
import cn.chanel.mall.enums.ResultEnum;
import cn.chanel.mall.exception.MallException;
import cn.chanel.mall.service.CategoryService;
import cn.chanel.mall.service.GoodsService;
import cn.chanel.mall.utils.UploadUtils;
import cn.chanel.mall.vo.GoodsVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/admin/goods")
public class AdminGoodsController {
@Resource
private AdminAccountConfig adminAccountConfig;
    @Resource
    private GoodsService goodsService;

    @Resource
    private CategoryService categoryService;

    @GetMapping("list")
    public ModelAndView list(@RequestParam(required = false) String key, Map<String, Object> map) {
        List<GoodsVO> allGoods;
        if(StringUtils.hasText(key)){
            allGoods = goodsService.getAllGoodsByKey(key);
        } else {
            allGoods = goodsService.getAllGoods();
        }
        map.put("allGoods", allGoods);
        return new ModelAndView("/admin/goods/list", map);
    }

    @PostMapping("add")
    public ModelAndView add(@RequestParam(value = "file", required = false) MultipartFile file,
                            GoodsForm goodsForm) {
        if(file == null || file.isEmpty()){
            throw new MallException(ResultEnum.FILE_UPLOAD_NULL);
        }
        final String generateFileName = UploadUtils.generateFileName(file.getName());
        File destFile = new File(FileUtils.getTempDirectory(), generateFileName);
        if(destFile.exists()) {
            boolean delete = destFile.delete();
            log.info("【AdminGoodsController】add tmp.zip delete result: {}", delete);
        }
        try {
            file.transferTo(destFile);
        } catch (IOException e) {
            log.error("【AdminGoodsController】add", e);
        }
        goodsService.addNewGoods(generateFileName, goodsForm);
        return new ModelAndView("redirect:/admin/goods/list");
    }

    @GetMapping("add")
    public ModelAndView add(Map<String, Object> map) {
        map.put("allCategories", categoryService.getAllCategory());
        return new ModelAndView("/admin/goods/add");
    }

    @GetMapping("update")
    public ModelAndView update(@RequestParam String goods, Map<String, Object> map) {
        map.put("allCategories", categoryService.getAllCategory());
        if(StringUtils.hasText(goods)){
            map.put("goods", goodsService.getGoodsById(goods));
        }
        return new ModelAndView("/admin/goods/update");
    }

    @PostMapping("update")
    public ModelAndView add(@RequestParam(value = "file", required = false) MultipartFile file,
                            GoodsUpdateForm goodsForm) {
        if(file == null || file.isEmpty()){
            // 使用原图
            goodsService.updateGoods(null, goodsForm);
        }else {
            // 使用新图
            final String generateFileName = UploadUtils.generateFileName(file.getName());
            File destFile = new File(FileUtils.getTempDirectory(), generateFileName);
            if(destFile.exists()) {
                var deleteResult = destFile.delete();
                log.info("deleteResult = " + deleteResult);
            }
            try {
                file.transferTo(destFile);
            } catch (IOException e) {
                log.error("updateGoods", e);
            }
            goodsService.updateGoods(generateFileName, goodsForm);
        }
        return new ModelAndView("redirect:/admin/goods/list");
    }
}

