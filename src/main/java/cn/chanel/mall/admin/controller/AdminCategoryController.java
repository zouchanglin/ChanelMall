package cn.chanel.mall.admin.controller;

import cn.chanel.mall.service.CategoryService;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/admin/category")
public class AdminCategoryController {

    @Resource
    private CategoryService categoryService;

    @GetMapping("list")
    public ModelAndView list(Map<String, Object> map) {
        map.put("allCategory", categoryService.getAllCategory());
        return new ModelAndView("/admin/category/list", map);
    }

    @PostMapping("add")
    public ModelAndView add(@RequestParam String name, @RequestParam String describe) {
        if(StringUtils.hasText(name)){
            categoryService.addNewCategory(name, describe);
        }
        return new ModelAndView("redirect:/admin/category/list");
    }

    @GetMapping("add")
    public ModelAndView add() {
        return new ModelAndView("/admin/category/add");
    }
}

