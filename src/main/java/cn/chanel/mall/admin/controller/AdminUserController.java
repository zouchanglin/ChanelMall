package cn.chanel.mall.admin.controller;

import cn.chanel.mall.admin.vo.AdminUserVO;
import cn.chanel.mall.service.UserService;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/user")
public class AdminUserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    @GetMapping("list")
    public ModelAndView user(@RequestParam(required = false) String key, Map<String, Object> map) {
        List<AdminUserVO> allUser;
        if(StringUtils.hasText(key)){
            allUser = userService.getAllUserByKey(key);
        } else {
            allUser = userService.getAllUser();
        }
        map.put("allUser", allUser);
        return new ModelAndView("/admin/user/list", map);
    }
}

