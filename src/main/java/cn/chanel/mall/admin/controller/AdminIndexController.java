package cn.chanel.mall.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminIndexController {
    @GetMapping
    public ModelAndView index(){
        return new ModelAndView("redirect:/admin/user/list");
    }
}
