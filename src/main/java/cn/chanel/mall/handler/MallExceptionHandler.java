package cn.chanel.mall.handler;

import cn.chanel.mall.exception.MallException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@ControllerAdvice
public class MallExceptionHandler {
    @ExceptionHandler(value = MallException.class)
    public ModelAndView handlerAdminException(MallException e){
        final HashMap<String, Object> map = new HashMap<>();
        map.put("msg", e.getMessage());
        map.put("url", "/");
        return new ModelAndView("/admin/common/error", map);
    }
}
