package cn.chanel.mall.constant;

public interface CookieName {
    /**
     * 用户登录之后的Cookie Key
     */
    String USER_LOGIN = "user_login_cookie";

    /**
     * 管理员登录
     */
    String ADMIN_LOGIN = "admin_login_key";
}
