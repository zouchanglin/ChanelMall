package cn.chanel.mall.constant;

public interface PrefixUrl {
    /**
     * 图片流服务URL前缀
     */
    String PREFIX_URL = "http://localhost:8080/file/";
}
