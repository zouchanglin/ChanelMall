package cn.chanel.mall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/pay")
public class PayController {

    @GetMapping
    public ModelAndView pay(@RequestParam String cartId, Map<String, Object> map){
        map.put("cartId", cartId);
        return new ModelAndView("pay", map);
    }
}

