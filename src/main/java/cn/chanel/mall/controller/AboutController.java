package cn.chanel.mall.controller;

import cn.chanel.mall.constant.CookieName;
import cn.chanel.mall.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/about")
public class AboutController {
    @Resource
    private UserService userService;

    @GetMapping("us")
    public ModelAndView about(@CookieValue(name = CookieName.USER_LOGIN, required = false) String userId,
                              Map<String, Object> map){
        if(StringUtils.hasText(userId)) {
            map.put("userInfo", userService.findUserById(userId));
        }
        return new ModelAndView("about");
    }

    @GetMapping("contact")
    public ModelAndView contact(@CookieValue(name = CookieName.USER_LOGIN, required = false) String userId,
                                Map<String, Object> map){
        if(StringUtils.hasText(userId)) {
            map.put("userInfo", userService.findUserById(userId));
        }
        return new ModelAndView("contact");
    }
}
