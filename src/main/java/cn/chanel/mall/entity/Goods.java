package cn.chanel.mall.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * 商品信息
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Goods {
    @Id
    private String id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品库存
     */
    private long stock;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品图片链接
     */
    private String image;

    /**
     * 商品描述
     */
    private String describe;

    /**
     * 商品评分
     */
    private int score;

    /**
     * 商品类别
     */
    private String category;
}
