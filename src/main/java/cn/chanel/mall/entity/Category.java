package cn.chanel.mall.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Proxy;
import org.springframework.context.annotation.Lazy;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

/**
 * 商品分类
 */
@Data
@Entity
@Proxy(lazy = false)
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    @Id
    private String id;
    /**
     * 类别名称
     */
    private String name;

    /**
     * 类别描述
     */
    private String describe;
}
