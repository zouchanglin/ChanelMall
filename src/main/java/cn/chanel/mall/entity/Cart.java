package cn.chanel.mall.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;


/**
 * 购物车
 */
@Data
@Entity
public class Cart {
    @Id
    private String id;

    /**
     * 用户Id
     */
    private String user;

    /**
     * 商品Id
     */
    private String goods;

    /**
     * 商品数量
     */
    private int nums;
}
