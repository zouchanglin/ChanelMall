package cn.chanel.mall.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * 订单
 */
@Data
@Entity
public class Orders {
    @Id
    private String id;

    /**
     * 订单总金额
     */
    private BigDecimal money;

    /**
     * 订单状态（创建、已支付、已发货、已签收、已退款）
     */
    private int status;

    /**
     * 商品ID
     */
    private String goodId;

    /**
     * 商品数量
     */
    private int num;

    /**
     * 订单创建时间戳
     */
    private long createTime;

    /**
     * 订单更新时间戳
     */
    private long updateTime;

    /**
     * 用户
     */
    private String userId;
}
