package cn.chanel.mall.utils;

import org.springframework.util.StringUtils;

public class UploadUtils {
    public static String generateFileName(String name) {
        String suffix = "";
        if(StringUtils.hasText(name)){
            if(name.endsWith("jpg")){
                suffix = "jpg";
            } else if(name.endsWith("png")){
                suffix = "png";
            } else if(name.endsWith("jpeg")){
                suffix = "jpeg";
            }
        }
        return System.currentTimeMillis() + "." + suffix;
    }
}
