package cn.chanel.mall.utils;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataFormatUtil {
    public static final SimpleDateFormat simpleDateFormat =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String formatDate(long time){
        return simpleDateFormat.format(new Date(time));
    }
}
