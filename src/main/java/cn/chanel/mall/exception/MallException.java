package cn.chanel.mall.exception;


import cn.chanel.mall.enums.ResultEnum;
import lombok.Getter;

@Getter
public class MallException extends RuntimeException{
    private final Integer code;

    public MallException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public MallException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
