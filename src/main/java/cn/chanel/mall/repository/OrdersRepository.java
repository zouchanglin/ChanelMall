package cn.chanel.mall.repository;

import cn.chanel.mall.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Orders, String> {
    List<Orders> findAllByUserId(String userId);

    List<Orders> findAllByIdContains(String id);
}
