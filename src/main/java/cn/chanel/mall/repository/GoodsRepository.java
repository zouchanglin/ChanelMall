package cn.chanel.mall.repository;

import cn.chanel.mall.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodsRepository extends JpaRepository<Goods, String> {
    List<Goods> findAllByCategory(String category);

    List<Goods> findAllByNameContains(String name);

    List<Goods> findAllByCategoryAndNameContains(String category, String key);
}
