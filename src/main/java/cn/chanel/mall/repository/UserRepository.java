package cn.chanel.mall.repository;

import cn.chanel.mall.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {
    User findUserByEmailAndPassword(String email, String password);

    User findUserByEmail(String email);

    List<User> findAllByIdContainsOrEmailContains(String id, String email);
}
