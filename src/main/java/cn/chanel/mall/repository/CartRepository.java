package cn.chanel.mall.repository;

import cn.chanel.mall.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, String> {
    List<Cart> findAllByUser(String user);

    Cart findCartByUserAndGoods(String user, String goods);
}
